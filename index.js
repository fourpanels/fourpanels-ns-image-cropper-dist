export * from './imagecropper';
export var GestureTypes;
(function (GestureTypes) {
    GestureTypes[GestureTypes["NONE"] = 0] = "NONE";
    GestureTypes[GestureTypes["SCALE"] = 1] = "SCALE";
    GestureTypes[GestureTypes["ROTATE"] = 2] = "ROTATE";
    GestureTypes[GestureTypes["ALL"] = 3] = "ALL";
})(GestureTypes || (GestureTypes = {}));
