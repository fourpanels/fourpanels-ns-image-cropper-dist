export * from './imagecropper';
import { ImageSource } from '@nativescript/core';
export declare enum GestureTypes {
    NONE = 0,
    SCALE = 1,
    ROTATE = 2,
    ALL = 3
}
export interface OptionsCommon {
    width?: number;
    height?: number;
    lockSquare?: boolean;
    circularCrop?: boolean;
}
export interface AspectRatio {
    aspectRatioTitle: string;
    aspectRatioX: number;
    aspectRatioY: number;
}
export interface AspectRatioOptions {
    defaultIndex: number;
    aspectRatios: AspectRatio[];
}
export interface OptionsAndroid {
    isFreeStyleCropEnabled?: boolean;
    toolbarTitle?: string;
    toolbarTextColor?: string;
    toolbarColor?: string;
    rootViewBackgroundColor?: string;
    logoColor?: string;
    statusBarColor?: string;
    showCropGrid?: boolean;
    showCropFrame?: boolean;
    cropFrameStrokeWidth?: number;
    cropGridStrokeWidth?: number;
    cropGridColor?: string;
    cropFrameColor?: string;
    cropGridRowCount?: number;
    cropGridColumnCount?: number;
    hideBottomControls?: boolean;
    compressionQuality?: number;
    dimmedLayerColor?: string;
    setAspectRatioOptions?: AspectRatioOptions;
    toolbarCropDrawable?: any;
    toolbarCancelDrawable?: any;
    setAllowedGestures?: [GestureTypes, GestureTypes, GestureTypes];
}
export interface Result {
    response: 'Success' | 'Error' | 'Cancelled';
    image: ImageSource | null;
}
export declare class ImageCropper {
    show(image: ImageSource, options?: OptionsCommon, androidOptions?: OptionsAndroid): Promise<Result>;
}
