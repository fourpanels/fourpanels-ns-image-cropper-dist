import { ImageSource } from '@nativescript/core';
import { OptionsAndroid, OptionsCommon, Result } from './';
export declare class ImageCropper {
    show(image: ImageSource, options?: OptionsCommon, androidOptions?: OptionsAndroid): Promise<Result>;
    private static _gcd;
}
