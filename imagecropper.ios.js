import { Frame, ImageSource } from '@nativescript/core';
let _options;
var TOCropViewControllerDelegateImpl = /** @class */ (function (_super) {
    __extends(TOCropViewControllerDelegateImpl, _super);
    function TOCropViewControllerDelegateImpl() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TOCropViewControllerDelegateImpl.initWithOwner = function (owner) {
        // console.log("TOCropViewControllerDelegateImpl.initWithOwner");
        var handler = TOCropViewControllerDelegateImpl.new();
        handler._owner = owner;
        return handler;
    };
    TOCropViewControllerDelegateImpl.prototype.initResolveReject = function (resolve, reject) {
        // console.log("TOCropViewControllerDelegateImpl.initResolveReject");
        this._resolve = resolve;
        this._reject = reject;
    };
    TOCropViewControllerDelegateImpl.prototype.cropViewControllerDidCropToCircularImageWithRectAngle = function (cropViewController, image, cropRect, angle) {
        this.cropViewControllerDidCropToImageWithRectAngle(cropViewController, image, cropRect, angle);
    };
    TOCropViewControllerDelegateImpl.prototype.cropViewControllerDidCropToImageWithRectAngle = function (cropViewController, image, cropRect, angle) {
        // console.log("TOCropViewControllerDelegateImpl.cropViewControllerDidCropToImageWithRectAngle");
        cropViewController.dismissViewControllerAnimatedCompletion(true, null);
        if (image) {
            var imgSrc = new ImageSource();
            if (_options && _options.width && _options.height) {
                // Resize Image
                var rect = CGRectMake(0, 0, _options.width, _options.height);
                UIGraphicsBeginImageContext(rect.size);
                image.drawInRect(rect);
                var resizedImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                try {
                    imgSrc.setNativeSource(resizedImage);
                }
                catch (e) {
                    console.error(e);
                }
                if (imgSrc.ios) {
                    this._resolve({
                        response: "Success",
                        image: imgSrc
                    });
                }
                else {
                    this._reject({
                        response: "Error",
                        image: null
                    });
                }
            }
            else {
                // Use Cropped Image w/o Resize
                try {
                    imgSrc.setNativeSource(image);
                }
                catch (e) {
                    console.error(e);
                }
                if (imgSrc.ios) {
                    this._resolve({
                        response: "Success",
                        image: imgSrc
                    });
                }
                else {
                    this._reject({
                        response: "Error",
                        image: null
                    });
                }
            }
        }
        CFRelease(cropViewController.delegate);
        // return;
    };
    TOCropViewControllerDelegateImpl.prototype.cropViewControllerDidFinishCancelled = function (cropViewController, cancelled) {
        // console.log("TOCropViewControllerDelegateImpl.cropViewControllerDidFinishCancelled");
        cropViewController.dismissViewControllerAnimatedCompletion(true, null);
        this._resolve({
            response: "Cancelled",
            image: null
        });
        CFRelease(cropViewController.delegate);
        // return;
    };
    TOCropViewControllerDelegateImpl.ObjCProtocols = [TOCropViewControllerDelegate];
    return TOCropViewControllerDelegateImpl;
}(NSObject));
export class ImageCropper {
    show(image, options = {}, androidOptions = {}) {
        return new Promise((resolve, reject) => {
            _options = options;
            if (image.ios) {
                const viewController = TOCropViewController.alloc().initWithCroppingStyleImage(options.circularCrop ? 1 : 0, image.ios);
                const delegate = TOCropViewControllerDelegateImpl.initWithOwner(new WeakRef(viewController));
                delegate.initResolveReject(resolve, reject);
                CFRetain(delegate);
                viewController.delegate = delegate;
                let vc = Frame.topmost().ios.controller;
                let page = null;
                while (vc.presentedViewController
                    && vc.presentedViewController.viewLoaded) {
                    vc = vc.presentedViewController;
                    if (!vc.beingDismissed)
                        page = vc;
                }
                if (page === null) {
                    page = vc;
                }
                if (_options.lockSquare) {
                    viewController.aspectRatioPreset = 1;
                    viewController.aspectRatioLockEnabled = true;
                    viewController.aspectRatioPickerButtonHidden = true;
                    viewController.resetAspectRatioEnabled = false;
                }
                page.presentViewControllerAnimatedCompletion(viewController, true, function () {
                    if (_options) {
                        if (_options.width && _options.height) {
                            const gcd = ImageCropper._gcd(_options.width, _options.height);
                            viewController.toolbar.clampButtonHidden = true;
                            viewController.cropView.setAspectRatioAnimated(CGSizeMake(_options.width / gcd, _options.height / gcd), false);
                        }
                    }
                });
            }
            else {
                reject({
                    response: "Error",
                    image: null
                });
            }
        });
    }
    static _gcd(width, height) {
        if (height === 0) {
            return width;
        }
        else {
            return ImageCropper._gcd(height, width % height);
        }
    }
}
